/* 
Javascript
PTB Assesment Week 4
*/

// Tuliskan sebuah fungsi yg mengembalikan nilai sesuai dengan "Expected output"

// Concatenate string with parameter
const umurSaya = (a) => {
  console.log();
  // Expected output : "Umur saya adalah 18"
};

umurSaya();

// Javascript Operator
const penambahan = (a, b) => {
  console.log();
  // Expected output : "4 + 5 = 9"
};

penambahan();

const pengurangan = (a, b) => {
  console.log();
  // Expected output : "10 - 5 = 5"
};

pengurangan();

const perkalian = (a, b) => {
  console.log();
  // Expected output : "3 x 5 = 15"
};

perkalian();

const pembagian = (a, b) => {
  console.log();
  // Expected output : "10 : 2 = 5"
};

pembagian();
